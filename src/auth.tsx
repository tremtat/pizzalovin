import { decode } from "jsonwebtoken";
import React, {
  ComponentType,
  Component,
  useState,
  ComponentProps,
  useEffect,
  JSXElementConstructor
} from "react";
import { AuthModal } from "./AuthModal";

export class AuthService {
  private readonly storageKey = "authToken";

  private get token(): string {
    return localStorage.getItem(this.storageKey) || "";
  }

  private set token(token: string) {
    localStorage.setItem(this.storageKey, token);
  }

  public getToken(): string {
    return this.token || "";
  }

  deleteToken() {
    localStorage.removeItem(this.storageKey);
  }

  isLoggedIn(): string | null {
    console.log("checking if user is logged in");
    const decodedToken = decode(this.token);
    console.log(decodedToken);

    if (
      !decodedToken ||
      typeof decodedToken === "string" ||
      !decodedToken.exp ||
      Number.isNaN(Number(decodedToken.exp))
    ) {
      return null;
    }

    if (Date.now() / 1000 > Number(decodedToken.exp)) {
      return null;
    }

    return decodedToken.username as string;
  }

  async logIn({ username, password }: { [key: string]: string }): Promise<any> {
    const res = await fetch("/api/login", {
      method: "POST",
      body: JSON.stringify({ username, password })
    })
    const body = await res.json();

    if (res.ok) {
      this.token = body.token;
    } else {
      throw new Error(body.message)
    }

  }

  logOut(): void {
    this.deleteToken();
    window.location.reload();
  }

  async signUp({ username, password }: { [key: string]: string }): Promise<any> {
    const res = await fetch("/api/signup", {
      method: "POST",
      body: JSON.stringify({ username, password })
    })
    const body = await res.json();

    if (res.ok) {
      this.token = body.token;
    } else {
      throw new Error(body.message)
    }

  }
}

export const auth = new AuthService();

interface ModalState {
  isOpen: boolean;
  errorText: string;
}

export function withAuth(Component: any) {
  return (props: any) => {
    const [modalState, setModalState] = useState<ModalState>({
      isOpen: true,
      errorText: ""
    });
    const [username, setUsername] = useState<string>("");

    useEffect(() => {
      checkLoginState();
    }, []);

    function checkLoginState() {
      const loggedUser = auth.isLoggedIn();
      if (loggedUser) {
        setModalState({ ...modalState, ...{ isOpen: false } });
        setUsername(loggedUser);
      } else {
        setModalState({ ...modalState, ...{ isOpen: true } });
      }
    }

    function handleLogin({ username, password }: { [key: string]: string }) {
      auth
        .logIn({ username, password })
        .then(() => {
          checkLoginState();
        })
        .catch(err => {
          handleError(err.message);
        });
    }

    function handleSignup({ username, password }: { [key: string]: string }) {
      auth
        .signUp({ username, password })
        .then(() => {
          checkLoginState();
        })
        .catch((err: Error) => {
          handleError(err.message);
        });
    }

    function handleError(errorText: string) {
      setModalState({ ...modalState, ...{ errorText: errorText } });
    }

    function logout(): void {
      auth.deleteToken();
      window.location.reload();
    }

    return (
      <>
        <Component
          handleLogout={logout}
          loggedIn={!modalState.isOpen}
          username={username}
          {...props}
        />
        {modalState.isOpen && (
          <AuthModal
            handleLogin={handleLogin}
            handleSignup={handleSignup}
            handleError={handleError}
            errorText={modalState.errorText}
          />
        )}
      </>
    );
  };
}
