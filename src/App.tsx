import React, { useState, useEffect } from "react";
import { auth, withAuth } from "./auth";
import {
  makeStyles,
  AppBar,
  Toolbar,
  IconButton,
  Button,
  Typography
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { Pizza } from "./Pizza";
import { UsersScore } from "./UsersScore";

const useStyles = makeStyles({
  grow: {
    flexGrow: 1
  },
  appContainer: {
    backgroundColor: "#282c34",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    fontSize: "calc(10px + 2vmin)",
    color: "white"
  }
});

export interface UsersState {
  username: string;
  likes: number;
}

interface Props {
  handleLogout: () => any;
  loggedIn: boolean;
  username: string;
}

export const BaseApp = ({ handleLogout, loggedIn, username }: Props) => {
  const [users, setUsers] = useState<UsersState[]>([]);
  const [isSideBarOpen, toggleSideBar] = useState<boolean>(false);

  const styles = useStyles();

  useEffect(() => {
    if (loggedIn && users.length === 0) {
      getUsers();
    }
  });

  function getUsers() {
    fetch("/api/getallusers")
      .then(res => res.json())
      .then(resBody => {
        setUsers(resBody);
      })
      .catch(err => {
        setUsers([]);
      });
  }

  function handlePizzaLove() {
    fetch("/api/pizza", {
      method: "POST",
      headers: { Authorization: `Bearer ${auth.getToken()}` }
    }).then(() => {
      getUsers();
    });
  }

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={e => toggleSideBar(!isSideBarOpen)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">{username}</Typography>
          <div className={styles.grow} />
          <Button color="inherit" onClick={handleLogout}>
            Logout
          </Button>
        </Toolbar>
      </AppBar>
      {users.length > 0 && (
        <UsersScore
          isOpen={isSideBarOpen}
          users={users}
          close={() => toggleSideBar(false)}
        />
      )}
      <div className={styles.appContainer}>
        <Pizza handleClick={handlePizzaLove} />
      </div>
    </>
  );
};

const App = withAuth(BaseApp) as (props: any) => JSX.Element;

export default App;
