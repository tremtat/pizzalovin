import React, { ComponentType, useEffect, useState } from "react";
import { Drawer, List, ListItem, ListItemText } from "@material-ui/core";
import { UsersState } from "./App";
import { makeStyles } from "@material-ui/styles";

interface Props {
  isOpen: boolean;
  users: UsersState[];
  close: () => any;
}

const useStyles = makeStyles({
  sidebar: {
    width: 250,
    padding: 15
  }
});

export function UsersScore({ isOpen, users, close }: Props) {
  const styles = useStyles();
  const renderListItem = (user: UsersState) => (
    <List key={user.username} className={styles.sidebar}>
      <ListItemText>
        <span>{user.likes}</span> <span>{user.username}</span>
      </ListItemText>
    </List>
  );
  return (
    <Drawer open={isOpen} onClose={close}>
      {users.map(user => renderListItem(user))}
    </Drawer>
  );
}
