import React, { useState } from "react";
import {
  DialogActions,
  DialogContent,
  Button,
  TextField
} from "@material-ui/core";

interface State {
  username: string;
  password: string;
  repeatPassword: string;
}

interface Props {
  handleSignup: ({ username, password }: { [key: string]: string }) => any;
  handleError: (msg: string) => any;
}

export function SignupForm({ handleSignup, handleError }: Props) {
  const [values, setValues] = useState<State>({
    username: "",
    password: "",
    repeatPassword: ""
  });

  function handleChange(name: keyof State) {
    return function(event: React.ChangeEvent<HTMLInputElement>) {
      setValues({ ...values, [name]: event.target.value });
    };
  }

  function handleSubmit() {
    const { username, password, repeatPassword } = values;
    if (password !== repeatPassword) {
      handleError("Passwords are different. Please try again");
    }
    handleSignup({ username, password });
  }
  return (
    <>
      <TextField
        autoFocus
        label="Username"
        value={values.username}
        type="text"
        fullWidth
        onChange={handleChange("username")}
      />
      <TextField
        label="Password"
        type="password"
        value={values.password}
        fullWidth
        onChange={handleChange("password")}
      />
      <TextField
        label="Password"
        type="password"
        value={values.repeatPassword}
        fullWidth
        onChange={handleChange("repeatPassword")}
      />
      <DialogActions>
        <Button onClick={handleSubmit}>Sign Up</Button>
      </DialogActions>
    </>
  );
}
