import React, { useState } from "react";
import {
  Dialog,
  DialogContentText,
  Tabs,
  Tab,
  DialogContent
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { LoginForm } from "./LoginForm";
import { SignupForm } from "./SignupForm";

interface Props {
  handleLogin: ({ username, password }: { [key: string]: string }) => any;
  handleSignup: ({ username, password }: { [key: string]: string }) => any;
  handleError: (errorText: string) => any;
  errorText: string;
}

const useStyles = makeStyles({
  errorText: {
    color: "red"
  }
});

export function AuthModal(props: Props) {
  const [tabPos, setTabPos] = React.useState<number>(0);
  const { errorText } = props;
  const styles = useStyles();

  function handleTabChange(event: React.ChangeEvent<{}>, newValue: number) {
    setTabPos(newValue);
  }

  return (
    <Dialog open={true}>
      <Tabs value={tabPos} onChange={handleTabChange} centered>
        <Tab label="Log In" />
        <Tab label="Sign Up" />
      </Tabs>
      <DialogContent>
        {errorText && (
          <DialogContentText className={styles.errorText}>
            {errorText}
          </DialogContentText>
        )}
        {tabPos === 0 && (
          <LoginForm
            handleLogin={props.handleLogin}
            handleError={props.handleError}
          />
        )}
        {tabPos === 1 && (
          <SignupForm
            handleSignup={props.handleSignup}
            handleError={props.handleError}
          />
        )}
      </DialogContent>
    </Dialog>
  );
}
