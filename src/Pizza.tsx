import React, { useState } from "react";
import { makeStyles } from "@material-ui/core";
import pizza from "./pizza.png";

const useStyles = makeStyles({
  animate: {
    animationName: "$Pizza-Spinner",
    animationDuration: "250ms",
    animationIterationCount: 5,
    animationTimingFunction: "linear"
  },
  pizza: {
    height: "40vmin"
  },
  "@keyframes Pizza-Spinner": {
    "0%": {
      transform: "rotate(0deg)"
    },
    "100%": {
      transform: "rotate(360deg)"
    }
  }
});

interface Props {
  handleClick: () => any;
}

interface AnimationState {
  isSpinning: boolean;
}

export function Pizza({ handleClick }: Props) {
  const [animationState, setAnimationState] = useState<AnimationState>({
    isSpinning: false
  });
  const styles = useStyles();

  function onClick() {
    if (animationState.isSpinning) return;

    setAnimationState({ isSpinning: true });
    handleClick();
  }

  return (
    <>
      <img
        src={pizza}
        onClick={onClick}
        onAnimationEnd={e => setAnimationState({ isSpinning: false })}
        className={`${styles.pizza} ${
          animationState.isSpinning ? styles.animate : ""
        }`}
      />
    </>
  );
}
