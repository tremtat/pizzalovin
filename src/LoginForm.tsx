import React, { useState } from "react";
import {
  DialogActions,
  DialogContent,
  Button,
  TextField
} from "@material-ui/core";

interface State {
  username: string;
  password: string;
}

interface Props {
  handleLogin: ({ username, password }: { [key: string]: string }) => any;
  handleError: (msg: string) => any;
}

export function LoginForm({ handleLogin }: Props) {
  const [values, setValues] = useState<State>({
    username: "",
    password: ""
  });

  function handleChange(name: keyof State) {
    return function(event: React.ChangeEvent<HTMLInputElement>) {
      setValues({ ...values, [name]: event.target.value });
    };
  }

  function handleSubmit() {
    const { username, password } = values;
    handleLogin({ username, password });
  }

  return (
    <>
      <TextField
        autoFocus
        label="Username"
        value={values.username}
        type="text"
        fullWidth
        onChange={handleChange("username")}
      />
      <TextField
        label="Password"
        type="password"
        value={values.password}
        fullWidth
        onChange={handleChange("password")}
      />
      <DialogActions>
        <Button onClick={handleSubmit}>Log In</Button>
      </DialogActions>
    </>
  );
}
