import { NowRequest, NowResponse } from "@now/node";
import { connectDB } from "../db";
import { User } from "../user.model";
import { verify } from "jsonwebtoken";

const secret = process.env.JWT_SECRET as string;

export default async (req: NowRequest, res: NowResponse) => {
  try {
    await connectDB();

    const users = await User.find();
    const toSend = users
      .map(user => ({ username: user.username, likes: user.likes }))
      .sort((userA, userB) => {
        return userA.likes - userB.likes;
      });
    res.status(200).send(toSend);
  } catch (err) {
    console.error(err);
    res
      .status(500)
      .send({
        status: 500,
        message:
          "An unexpected server error occurred. Please contact the maintainer of this app."
      });
  }
};
