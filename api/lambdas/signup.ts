import { NowRequest, NowResponse } from "@now/node";
import { json } from "micro";
import { connectDB } from "../db";
import { User } from "../user.model";
import { sign } from "jsonwebtoken";

interface Body {
  [key: string]: any;
  username: string;
  password: string;
}

const secret = process.env.JWT_SECRET as string;

export default async (req: NowRequest, res: NowResponse) => {
  try {
    await connectDB();
    const { username, password } = (await json(req)) as Body;
    const exists = await User.findOne({ username });
    if (exists) {
      return res.status(503).send({ message: "User exists already!" });
    }
    const newUser = await User.create({ username, password, likes: 0 });
    const token = sign(
      { _id: newUser._id, username: newUser.username },
      secret,
      {
        expiresIn: "10m"
      }
    );
    return res.status(200).send({ username: newUser.username, token });
  } catch (err) {
    res.status(503).send({
      message:
        "An unexpected server error occurred. Please contact the maintainer of this app."
    });
  }
};
