import { NowRequest, NowResponse } from "@now/node";
import { json, send } from "micro";
import { connectDB } from "../db";
import { User } from "../user.model";
import jwt from "jsonwebtoken";

interface Body {
  [key: string]: any;
  username: string;
  password: string;
}

const secret = process.env.JWT_SECRET as string;

export default async (req: NowRequest, res: NowResponse) => {
  try {
    await connectDB();
    const { username, password } = (await json(req)) as Body;
    const foo = await User.findOne({ username });
    if (!foo) {
      res
        .status(401)
        .send({
          status: 401,
          message: "There is no user with that username registered"
        });
    } else if (foo.password !== password) {
      res.status(401).send({ status: 401, message: "Wrong password" });
    } else {
      const token = jwt.sign({ _id: foo._id, username: foo.username }, secret, {
        expiresIn: "10m"
      });
      res.status(200).send({ username: foo.username, token });
    }
  } catch (err) {
    res
      .status(500)
      .send({
        status: 500,
        message:
          "An unexpected server error occurred. Please contact the maintainer of this app."
      });
  }
};
