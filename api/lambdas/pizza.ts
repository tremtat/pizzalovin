import { NowRequest, NowResponse } from "@now/node";
import { connectDB } from "../db";
import { User } from "../user.model";
import { verify } from "jsonwebtoken";

const secret = process.env.JWT_SECRET as string;

export default async (req: NowRequest, res: NowResponse) => {
  try {
    await connectDB();
    const authHeaders = req.headers.authorization;
    if (!authHeaders || authHeaders.split(" ")[0] !== "Bearer") {
      return res
        .status(403)
        .send({ message: "User is not allowed to perform this request" });
    }
    const token = authHeaders.split(" ")[1];
    const payload: any = verify(token, secret);
    await User.findOneAndUpdate(
      { username: payload.username },
      { $inc: { likes: 1 } }
    );
    res.status(204).send({ message: "sucessfully incremented" });
  } catch (err) {
    console.error(err);
    res.status(500).send({
      status: 500,
      message:
        "An unexpected server error occurred. Please contact the maintainer of this app."
    });
  }
};
