import mongoose from "mongoose";

let cachedDb: typeof mongoose | null = null;

export function connectDB(): Promise<typeof mongoose> {
  if (cachedDb) {
    return Promise.resolve(cachedDb);
  }
  return mongoose.connect(process.env.MONGODB_URI as string, {
    useNewUrlParser: true
  });
}
