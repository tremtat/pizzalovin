import { Schema, Document, model, Model } from "mongoose";

export interface UserDocument extends Document {
  username: string;
  password: string;
  likes: number
}

const userSchema: Schema<UserDocument> = new Schema({
  username: { type: String, unique: true },
  password: String,
  likes: Number
});

export const User = model<UserDocument>("User", userSchema);
